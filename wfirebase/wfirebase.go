/*
	wrapper library
	Copyright (C) 2020 RedPanda

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package wfirebase

import (
	"context"
	"time"

	firebase "firebase.google.com/go"
)

/* Wrapper type */
type WrapperFirebase struct {
	app     *firebase.App
	lastTry time.Time
}

/* Opens firebase app */
func (wrapperFirebase *WrapperFirebase) open() error {
	/* TODO: compare last connection try */

	wrapperFirebase.lastTry = time.Now()

	var err error
	wrapperFirebase.app, err = firebase.NewApp(context.Background(), nil)

	if err != nil {
		wrapperFirebase.app = nil
		return err
	}

	return nil
}

/* Returns firebase app */
func (wrapperFirebase *WrapperFirebase) App() (*firebase.App, error) {
	if wrapperFirebase.app == nil {
		err := wrapperFirebase.open()
		return wrapperFirebase.app, err
	}
	return wrapperFirebase.app, nil
}

/* Creates a new firebase wrapper */
func NewWrapperFirebase() (*WrapperFirebase, error) {
	wrapperFirebase := new(WrapperFirebase)

	return wrapperFirebase, wrapperFirebase.open()
}
