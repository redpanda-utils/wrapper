/*
   wrapper library
   Copyright (C) 2020 RedPanda

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package wpsql

import (
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

/* Wrapper type */
type WrapperPsql struct {
	conn     *gorm.DB
	lastTry  time.Time
	host     string
	port     string
	name     string
	schema   string
	user     string
	password string
}

/* Opens the database connection */
func (wrapperPsql *WrapperPsql) open() error {
	/* TODO: compare last connection try */

	if wrapperPsql.schema != "" {
		gorm.DefaultTableNameHandler =
			func(db *gorm.DB, defaultTableName string) string {
				return wrapperPsql.schema + "." + defaultTableName
			}
	}

	wrapperPsql.lastTry = time.Now()

	var err error
	/* TODO: implement ssl */
	wrapperPsql.conn, err = gorm.Open(
		"postgres",
		"host="+wrapperPsql.host+
			" port="+wrapperPsql.port+
			" user="+wrapperPsql.user+
			" dbname="+wrapperPsql.name+
			" sslmode=disable"+
			" password="+wrapperPsql.password,
	)

	if err != nil {
		wrapperPsql.conn = nil
		return err
	}

	return nil
}

func (wrapperPsql *WrapperPsql) Close() {
	if wrapperPsql.conn != nil {
		wrapperPsql.conn.Close()
	}
}

/* Returns database connection */
func (wrapperPsql *WrapperPsql) Conn() (*gorm.DB, error) {
	if wrapperPsql.conn == nil {
		err := wrapperPsql.open()
		return wrapperPsql.conn, err
	}
	return wrapperPsql.conn, nil
}

/* Create a new postgresql wrapper */
func NewWrapperPsql(host, port, name, schema, user,
	password string) (*WrapperPsql, error) {
	wrapperPsql := new(WrapperPsql)

	wrapperPsql.host = host
	wrapperPsql.port = port
	wrapperPsql.name = name
	wrapperPsql.schema = schema
	wrapperPsql.user = user
	wrapperPsql.password = password

	return wrapperPsql, wrapperPsql.open()
}
