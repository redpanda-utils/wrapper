module gitlab.com/redpanda-utils/wrapper

go 1.14

require (
	firebase.google.com/go v3.12.0+incompatible
	github.com/confluentinc/confluent-kafka-go v1.4.2 // indirect
	github.com/googleapis/gax-go v1.0.3 // indirect
	github.com/jinzhu/gorm v1.9.12
	go.opencensus.io v0.22.3 // indirect
	google.golang.org/api v0.20.0 // indirect
	google.golang.org/grpc v1.28.0
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.4.2
)
