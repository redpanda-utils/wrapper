/*
   wrapper library
   Copyright (C) 2020 RedPanda

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package wkafka

import (
	"errors"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

/* Wrapper type */
type WrapperKafka struct {
	consumer *kafka.Consumer
	producer *kafka.Producer
}

/* Returns kafka consumer */
func (wrapperKafka *WrapperKafka) Consumer() (*kafka.Consumer, error) {
	if wrapperKafka.consumer == nil {
		return nil, errors.New("consummer not openned")
	}
	return wrapperKafka.consumer, nil
}

/* Returns kafka producer */
func (wrapperKafka *WrapperKafka) Producer() (*kafka.Producer, error) {
	if wrapperKafka.producer == nil {
		return nil, errors.New("producer not openned")
	}
	return wrapperKafka.producer, nil
}

/* Creates a kafka consumer */
func newKafkaConsumer(wrapperKafka *WrapperKafka, server, groupId,
	autoOffsetReset string) error {
	if wrapperKafka.consumer != nil {
		return errors.New("consumer already existing")
	}

	var err error
	wrapperKafka.consumer, err = kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": server,
		"group.id":          groupId,
		"auto.offset.reset": autoOffsetReset,
	})

	return err
}

/* Creates a new kafka wrapper with consumer */
func NewWrapperKafkaConsumer(server, groupId,
	autoOffsetReset string) (*WrapperKafka, error) {
	wrapperKafka := new(WrapperKafka)

	if err := newKafkaConsumer(wrapperKafka, server, groupId,
		autoOffsetReset); err != nil {
		return nil, err
	}

	return wrapperKafka, nil
}

/* Creates a new kafka wrapper with producer */
func NewWrapperKafkaProducer() (*WrapperKafka, error) {
	return nil, errors.New("kafka producer not implemented")
}

/* Creates a new kafka wrapper with both consumer and producer */
func NewWrapperKafka() (*WrapperKafka, error) {
	return nil, errors.New("kafka producer not implemented")
}

/* Closes kafka connections */
func (wrapperKafka *WrapperKafka) Close() {
	if wrapperKafka.consumer != nil {
		wrapperKafka.consumer.Close()
	}

	if wrapperKafka.producer != nil {
		wrapperKafka.producer.Close()
	}
}
