# Wrapper
> Application wrapper library

Wrapper is a golang library to wrap objects into one bigger structure and handle
dynamic openning on usage.

* [Usage](#usage)
* [Firebase App](#firebase-app)
* [Grpc Server](#grpc-server)
* [Postgresql Connection](#postgresql-connection)

## Usage

Create a raw wrapper object

```go
package main

import "gitlab.com/redpanda-utils/wrapper"

func main() {
	w := wrapper.NewWrapper()
}
```

This object does not have any subojects. You can customize your wrapper by
issuing the proper methods.

## Firebase App

Add firebase app:

```go
package main

import "gitlab.com/redpanda-utils/wrapper"

func main() {
	w := wrapper.NewWrapper()

	/* Add firebase app */
	w.OpenFirebaseApp()

	/* Use the method FirebaseApp() to obtain the firebase app */
}
```

## Grpc Server

Add grpc server:

```go
package main

import "gitlab.com/redpanda-utils/wrapper"

func main() {
	w := wrapper.NewWrapper()

	/* Add grpc server */
	w.OpenGrpcServer("0.0.0.0", "5012")
	
	/* For a TLS grpc server use this function */
	/* w.OpenGrpcTLSServer("0.0.0.0", "5012", "cert.pem", "key.pem") */

	/* Use the method GrpcServer() to obtain the grpc server and set the proper
		endpoints */

	/* Serve */
	w.ServeGrpcServer()
}
```

## Postgresql Connection

Add postgresql connection:

```
package main

import "gitlab.com/redpanda-utils/wrapper"

func main() {
	w := wrapper.NewWrapper()

	/* Add psql connection */
	w.OpenPsqlConn(host, port, database_name, schema, user, password)

	/* Use the method PsqlConn() to obtain postgresql connection */

	/* Close */
	w.ClosePsqlConn()
}
```
