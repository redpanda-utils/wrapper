/*
   wrapper library
   Copyright (C) 2020 RedPanda

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package wrapper

import (
	"errors"

	firebase "firebase.google.com/go"
	"github.com/jinzhu/gorm"
	"gitlab.com/redpanda-utils/wrapper/wfirebase"
	"gitlab.com/redpanda-utils/wrapper/wgrpc"
	"gitlab.com/redpanda-utils/wrapper/wkafka"
	"gitlab.com/redpanda-utils/wrapper/wpsql"
	"google.golang.org/grpc"
	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

/* Wrapper type */
type Wrapper struct {
	firebaseApp *wfirebase.WrapperFirebase
	grpcServer  *wgrpc.WrapperGrpc
	kafkaConn   *wkafka.WrapperKafka
	psqlConn    *wpsql.WrapperPsql
}

/* Returns grpc server */
func (wrapper *Wrapper) GrpcServer() (*grpc.Server, error) {
	if wrapper.grpcServer == nil {
		return nil, errors.New("error grpc server not openned")
	}
	return wrapper.grpcServer.Server(), nil
}

/* Opens grpc server */
func (wrapper *Wrapper) OpenGrpcServer(ifc, port string) error {
	if wrapper.grpcServer == nil {
		var err error
		wrapper.grpcServer, err = wgrpc.NewWrapperGrpc(ifc, port)
		return err
	} else {
		return nil
	}
}

/* Opens grpc TLS server */
func (wrapper *Wrapper) OpenGrpcTLSServer(ifc, port, cert, key string) error {
	if wrapper.grpcServer == nil {
		var err error
		wrapper.grpcServer, err = wgrpc.NewWrapperGrpcTLS(ifc, port, cert, key)
		return err
	} else {
		return nil
	}
}

/* Serves grpc server */
func (wrapper *Wrapper) ServeGrpcServer() error {
	if wrapper.grpcServer == nil {
		return errors.New("error grpc server not openned")
	}

	wrapper.grpcServer.Serve()

	return nil
}

/* Returns kafka connection consumer */
func (wrapper *Wrapper) KafkaConnConsumer() (*kafka.Consumer, error) {
	if wrapper.kafkaConn == nil {
		return nil, errors.New("error kafka connection not openned")
	}
	return wrapper.kafkaConn.Consumer()
}

/* Returns kafka connection producer */
func (wrapper *Wrapper) KafkaConnProducer() (*kafka.Producer, error) {
	if wrapper.kafkaConn == nil {
		return nil, errors.New("error kafka connection not openned")
	}
	return wrapper.kafkaConn.Producer()
}

/* Opens kafka connection consumer */
func (wrapper *Wrapper) OpenKafkaConnConsumer(server, groupId,
	autoOffsetReset string) error {
	if wrapper.kafkaConn == nil {
		var err error
		wrapper.kafkaConn, err = wkafka.NewWrapperKafkaConsumer(server, groupId,
			autoOffsetReset)
		return err
	} else {
		return nil
	}
}

/* Opens kafka connection producer */
func (wrapper *Wrapper) OpenKafkaConnProducer() error {
	if wrapper.kafkaConn == nil {
		var err error
		wrapper.kafkaConn, err = wkafka.NewWrapperKafkaProducer()
		return err
	} else {
		return nil
	}
}

/* Opens kafka connection both consumer and producer */
func (wrapper *Wrapper) OpenKafkaConn() error {
	if wrapper.kafkaConn == nil {
		var err error
		wrapper.kafkaConn, err = wkafka.NewWrapperKafka()
		return err
	} else {
		return nil
	}
}

/* Closes kafka connection */
func (wrapper *Wrapper) CloseKafkaConn() {
	if wrapper.kafkaConn != nil {
		wrapper.kafkaConn.Close()
	}
}

/* Returns psql connection */
func (wrapper *Wrapper) PsqlConn() (*gorm.DB, error) {
	if wrapper.psqlConn == nil {
		return nil, errors.New("error postgresql connection not openned")
	}
	return wrapper.psqlConn.Conn()
}

/* Opens psql server */
func (wrapper *Wrapper) OpenPsqlConn(host, port, name, schema, user,
	password string) error {
	if wrapper.psqlConn == nil {
		var err error
		wrapper.psqlConn, err = wpsql.NewWrapperPsql(host, port, name, schema,
			user, password)
		return err
	} else {
		return nil
	}
}

/* Closes psql server */
func (wrapper *Wrapper) ClosePsqlConn() {
	if wrapper.psqlConn != nil {
		wrapper.psqlConn.Close()
	}
}

/* Returns firebase app */
func (wrapper *Wrapper) FirebaseApp() (*firebase.App, error) {
	if wrapper.firebaseApp == nil {
		return nil, errors.New("error firebase application not openned")
	}
	return wrapper.firebaseApp.App()
}

/* Opens firebase app */
func (wrapper *Wrapper) OpenFirebaseApp() error {
	if wrapper.firebaseApp == nil {
		var err error
		wrapper.firebaseApp, err = wfirebase.NewWrapperFirebase()
		return err
	} else {
		return nil
	}
}

/* Creates a new wrapper */
func NewWrapper() *Wrapper {
	wrapper := new(Wrapper)

	wrapper.firebaseApp = nil
	wrapper.grpcServer = nil
	wrapper.psqlConn = nil

	return wrapper
}
